import * as React from "react";
import "./App.css";
import {useEffect, useState} from "react";
import CircularProgress from '@mui/material/CircularProgress';
import {CryptocurrencyTable} from "./components/CryptocurrencyTable";

function App() {
    const [error, setError] = useState(null);
    const [loading, setLoading] = useState(true);
    const [items, setItems] = useState([]);

    useEffect(() => {
        const requestOptions = {
            method: 'GET',
            headers: {'X-CMC_PRO_API_KEY': 'b40854de-9a34-4e59-b83e-4cfa0b9acf6b'}
        };
        fetch("/v1/cryptocurrency/listings/latest?start=1&limit=5000&convert=USD", requestOptions)
            .then(res => res.json())
            .then(
                (result) => {
                    setItems(result.data);
                    setLoading(false);
                },
                (error) => {
                    setLoading(false);
                    setError(error);
                }
            )
    }, [])


    if (error) {
        return <div>Error: {error.message}</div>;
    } else {
        return (
            <div className="main">
                {loading ? <div className="loader"><CircularProgress/></div>
                    : <>
                        <p className="title">List of cryptocurrencies</p>
                        <CryptocurrencyTable data={items}/>
                    </>
                }
            </div>
        );
    }
}

export default App;
